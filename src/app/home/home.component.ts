import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  

  surveyForm=new FormGroup({
    reponse1Control:new FormControl(),
    reponse2Control:new FormControl(),
    reponse3Control:new FormControl()
  });

  constructor() { }

  print(){
    console.log(this.surveyForm.value);
    return false;
  }

  ngOnInit(): void {
  }

}
